export const canvas = document.getElementById('canvas');
export let variables = {
    // Set the context, 2D in this case.
    context: canvas.getContext('2d'),

    // Firework and particles collections.
    fireworks: [],
    particles: [],
    // Mouse coordinates.
    mouseX: 0,
    mouseY: 0,
    // Variable to check if mouse is down.
    isMouseDown: false,
    // Initial hue.
    hue: 120,
    // Track number of ticks since automated firework.
    ticksSinceFireworkAutomated: 0,
    // Track number of ticks since manual firework.
    ticksSinceFirework: 0,
};
