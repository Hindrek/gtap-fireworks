// Base firework acceleration.
// 1.0 causes fireworks to travel at a constant speed.
// Higher number increases the rate at which the firework accelerates over time.

export let config = {
    fireworkAcceleration: 1.05,
    // Minimum firework brightness.
    fireworkBrightnessMin: 60, //50
    // Maximum firework brightness.
    fireworkBrightnessMax: 80, //70
    // Base speed of fireworks.
    fireworkSpeed: 5,
    // Base length of firework trails.
    fireworkTrailLength: 4,
    // Minimum particle brightness.
    particleBrightnessMin: 50,
    // Maximum particle brightness.
    particleBrightnessMax: 95,
    // Base particle count per firework.
    particleCount: 150, // Default 80
    // Minimum particle decay rate.
    particleDecayMin: 0.015,
    // Maximum particle decay rate.
    particleDecayMax: 0.03,
    // Base particle friction.
    // Slows the speed of particles over time.
    particleFriction: 0.95,
    // Base particle gravity.
    // How quickly particiles move toward a downward trajectory.
    particleGravity: 0.7,
    // Variance in particle coloration.
    particleHueVariance: 20,
    // Base particle transparency.
    particleTransparency: 1,
    // Minimum particle speed.
    particleSpeedMin: 1,
    // Maximum particle speed.
    particleSpeedMax: 10,
    // Base length of explosion particle trails.
    particleTrailLength: 5,
    // Hue change per loop, used to rotate through different firework colors.
    hueStepIncrease: 0.5,
    // Minimum number of ticks per manual firework launch.
    ticksPerFireworkMin: 5,
    // Minimum number of ticks between each automatic firework launch.
    ticksPerFireworkAutomatedMin: 20,
    // Maximum number of ticks between each automatic firework launch.
    ticksPerFireworkAutomatedMax: 80,
    // Alpha level at which canvas cleanup iteration removes existing trails.
    // Lower value increases trail duration.
    canvasCleanupAlpha: 0.7, // Default 0.3
};

// === END CONFIGURATION ===
