// Get a random number within the specified range.
export const random = (min, max) => {
    return Math.random() * (max - min) + min;
};

//Calculate the distance between two points.
export const calculateDistance = (aX, aY, bX, bY) => {
    let xDistance = aX - bX;
    let yDistance = aY - bY;

    return Math.sqrt(Math.pow(xDistance, 2) + Math.pow(yDistance, 2));
};
