// imports
import '../sass/main.scss';
import './helpers.js';
import './app.js';
import '../img/foliage.svg';
import '../img/buildings.svg';
