import { random, calculateDistance } from './helpers';
import { canvas, variables } from './variables';
import { config } from './config';

// Set canvas dimensions.
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

// Canvas dynamic resize.
window.onresize = () => {
    var canvas = document.getElementById('canvas');
    canvas.width = window.innerWidth;
    canvas.style.width = window.innerWidth;
    canvas.height = window.innerHeight;
    canvas.style.height = window.innerHeight;
};

// Use requestAnimationFrames to maintain smooth animation loops.
// Fall back on setTimeout() if browser support isn't available.
window.requestAnimFrame = (() => {
    return (
        window.requestAnimationFrame ||
        window.webkitRequestAnimationframe ||
        window.mozRequestAnimationFrame ||
        function (callback) {
            window.setTimeout(callback, 1000 / 60);
        }
    );
})();

// === EVENT LISTENERS ===

// Track current mouse position within canvas.
canvas.addEventListener('mousemove', (e) => {
    variables.mouseX = e.pageX - canvas.offsetLeft;
    variables.mouseY = e.pageY - canvas.offsetTop;
});

// Track when mouse is pressed.
canvas.addEventListener('mousedown', (e) => {
    e.preventDefault();
    variables.isMouseDown = true;
});

// Track when mouse is released.
canvas.addEventListener('mouseup', (e) => {
    e.preventDefault();
    variables.isMouseDown = false;
});

// === END EVENT LISTENERS ===

// HANDLING FIREWORKS

// === PROTOTYPING ===

// Creates a new firework.
// Path begins at 'start' point and ends at 'end' point.
function Firework(startX, startY, endX, endY) {
    // Set current coordinates.
    this.x = startX;
    this.y = startY;
    // Set starting coordinates.
    this.startX = startX;
    this.startY = startY;
    // Set end coordinates.
    this.endX = endX;
    this.endY = endY;
    // Get the distance to the end point.
    this.distanceToEnd = calculateDistance(startX, startY, endX, endY);
    this.distanceTraveled = 0;
    // Create an array to track current trail particles.
    this.trail = [];
    // Trail length determines how many trailing particles are active at once.
    this.trailLength = config.fireworkTrailLength;
    // While the trail length remains, add current point to trail list.
    while (this.trailLength--) {
        this.trail.push([this.x, this.y]);
    }
    // Calculate the angle to travel from start to end point.
    this.angle = Math.atan2(endY - startY, endX - startX);
    // Set the speed.
    this.speed = config.fireworkSpeed;
    // Set the acceleration.
    this.acceleration = config.fireworkAcceleration;
    // Set the brightness.
    this.brightness = random(
        config.fireworkBrightnessMin,
        config.fireworkBrightnessMax
    );
    // Set the radius of click-target location.
    this.targetRadius = 2.5;
}

// Launch fireworks automatically.
const launchAutomatedFirework = () => {
    // Determine if the amount of ticks since the last automated launch is greater than random min/max values.
    if (
        variables.ticksSinceFireworkAutomated >=
        random(
            config.ticksPerFireworkAutomatedMin,
            config.ticksPerFireworkAutomatedMax
        )
    ) {
        // Check if mouse is currently not clicked.
        if (!variables.isMouseDown) {
            // Set start position to bottom center.
            const startX = canvas.width / 2;
            const startY = canvas.height;
            // Set end position to random position, somewhere in the top half of the screen.
            const endX = random(0, canvas.width);
            const endY = random(0, canvas.height / 2);
            // Create new firework and add to collection.
            variables.fireworks.push(new Firework(startX, startY, endX, endY));
            // Reset tick counter.
            variables.ticksSinceFireworkAutomated = 0;
        }
    } else {
        // Increment counter.
        variables.ticksSinceFireworkAutomated++;
    }
};

// Launch fireworks manually if mouse is pressed.
const launchManualFirework = () => {
    // Check if ticks since last firework launch is less than mnimum value.
    if (variables.ticksSinceFirework >= config.ticksPerFireworkMin) {
        // Check if mouse is down.
        if (variables.isMouseDown) {
            // Set start position to the bottom center.
            const startX = canvas.width / 2;
            const startY = canvas.height;
            // Set end position to current mouse position.
            const endX = variables.mouseX;
            const endY = variables.mouseY;
            // Create new firework and add to collection.
            variables.fireworks.push(new Firework(startX, startY, endX, endY));
            // Reset tick counter.
            variables.ticksSinceFirework = 0;
        }
    } else {
        // Increment counter.
        variables.ticksSinceFirework++;
    }
};

const launchMiddleFirework = () => {
    if (
        variables.ticksSinceFireworkAutomated >=
        random(
            config.ticksPerFireworkAutomatedMin,
            config.ticksPerFireworkAutomatedMax
        )
    ) {
        const startX = canvas.width / 2;
        const startY = canvas.height;
        const endX = canvas.width / 2;
        const endY = canvas.height / 2;
        variables.fireworks.push(new Firework(startX, startY, endX, endY));
        variables.ticksSinceFireworkAutomated = 0;
    } else {
        variables.ticksSinceFireworkAutomated++;
    }
};

const initiateAccidentalSkybeam = () => {
    const startX = canvas.width / 2;
    const startY = canvas.height;
    const endX = canvas.width / 2;
    const endY = canvas.height / 2;
    variables.fireworks.push(new Firework(startX, startY, endX, endY));
};

let redButton = false;
const intervalId = window.setInterval(function () {
    if (redButton) {
        initiateAccidentalSkybeam();
    }
}, 1);

// Update a firework prototype.
Firework.prototype.update = function (index) {
    // Remove the oldest trail particle.
    this.trail.pop();
    // Add the current position to the start of the trail.
    this.trail.unshift([this.x, this.y]);

    // Increase speed based on acceleration rate.
    this.speed *= this.acceleration;

    // Calculate current velocity for both x and y axes.
    const xVelocity = Math.cos(this.angle) * this.speed;
    const yVelocity = Math.sin(this.angle) * this.speed;
    // Calculate the current distance travelled based on starting position, current position, and velocity.
    // This can be used to determine if the firework has reached its final position.
    this.distanceTraveled = calculateDistance(
        this.startX,
        this.startY,
        this.x + xVelocity,
        this.y + yVelocity
    );

    // Check if final postition has been reached (or exceeded).
    if (this.distanceTraveled >= this.distanceToEnd) {
        // Destroy firework by removing it from the collection.
        variables.fireworks.splice(index, 1);
        // Create particle explosion at the end point. Important not to use this.x and this.y, since that position is always one loop behind.
        createParticles(this.endX, this.endY);
    } else {
        // End position hasn't been reached, so continue along the current trajectory by updating current coordinates.
        this.x += xVelocity;
        this.y += yVelocity;
    }
};

// Draw a firework.
// Use CanvasRenderingContext2D methods to create strokes as firework paths.
Firework.prototype.draw = function () {
    // Begin a new path for firework trail.
    variables.context.beginPath();
    // Get the coordinates for the oldest trail position.
    const trailEndX = this.trail[this.trail.length - 1][0];
    const trailEndY = this.trail[this.trail.length - 1][1];
    // Create a trail stroke from trail end to current firework position.
    variables.context.moveTo(trailEndX, trailEndY);
    variables.context.lineTo(this.x, this.y);
    // Set stroke coloration and style.
    // Use hue, saturation, and light values instead of RGB.
    variables.context.strokeStyle = `hsl(${variables.hue}, 100%, ${this.brightness}%)`;
    // Draw stroke.
    variables.context.stroke();
};

// Creates a new particle at provided 'x' and 'y' coordinates.
function Particle(x, y) {
    // Set current position.
    this.x = x;
    this.y = y;
    // To better simulate a firework, set the angle of travel to random value in any direction.
    this.angle = random(0, Math.PI * 2);
    // Set friction.
    this.friction = config.particleFriction;
    // Set gravity.
    this.gravity = config.particleGravity;
    // Set the hue to a somewhat randomized number.
    // This gives the particles within a firework explosion an appealing variance.
    this.hue = random(
        variables.hue - config.particleHueVariance,
        variables.hue + config.particleHueVariance
    );
    // Set brightness.
    this.brightness = random(
        config.particleBrightnessMin,
        config.particleBrightnessMax
    );
    // Set decay.
    this.decay = random(config.particleDecayMin, config.particleDecayMax);
    // Set speed.
    this.speed = random(config.particleSpeedMin, config.particleSpeedMax);
    // Create an array to track current tail particles.
    this.trail = [];
    // Trail length determines how many trailing particles are active at once.
    this.trailLength = config.particleTrailLength;
    // While the trail length remains, add current point to trail list.
    while (this.trailLength--) {
        this.trail.push([this.x, this.y]);
    }
    // Set transparency.
    this.transparency = config.particleTransparency;
}

// Update a particle prototype.
Particle.prototype.update = function (index) {
    // Remove the oldest trail particle.
    this.trail.pop();
    // Add the current position to the start of the trail.
    this.trail.unshift([this.x, this.y]);

    // Decrease speed based on friction rate.
    this.speed *= this.friction;
    // Calculate current position based on angle, speed and gravity (for y-axis only).
    this.x += Math.cos(this.angle) * this.speed;
    this.y += Math.sin(this.angle) * this.speed + this.gravity;

    // Apply transparency based on decay.
    this.transparency -= this.decay;
    // Use decay rate to determine if a particle should be destroyed.
    if (this.transparency <= this.decay) {
        // Destroy particle once transparency level is below decay.
        variables.particles.splice(index, 1);
    }
};

// Draw a particle.
// Use CanvasRenderingContext2D methods to create strokes as particle paths.
Particle.prototype.draw = function () {
    // Begin a new path for particle trail.
    variables.context.beginPath();
    // Get the coordinates for the oldest trail position.
    const trailEndX = this.trail[this.trail.length - 1][0];
    const trailEndY = this.trail[this.trail.length - 1][1];
    // Create a trail stroke from trail end to current particle position.
    variables.context.moveTo(trailEndX, trailEndY);
    variables.context.lineTo(this.x, this.y);
    // Set stroke coloration and style.
    // Use hue, brightness and transparency instead of RGBA.
    variables.context.strokeStyle = `hsla(${this.hue}, 100%, ${this.brightness}%, ${this.transparency})`;
    variables.context.stroke();
};

// === END PROTOTYPING ===

// === APPLICATION HELPERS ===

// Cleans up the canvas by removing older trails.
const cleanCanvas = () => {
    // Set 'destination-out' compsite mode, so that additional fill doesn't remove non-overlapping content.
    variables.context.globalCompositeOperation = 'destination-out';
    // Set alpha level of content to remove.
    // Lower value means that trails remain on screen longer.
    variables.context.fillStyle = `rgba(0, 0, 0, ${config.canvasCleanupAlpha})`;
    // Fill entire canvas.
    variables.context.fillRect(0, 0, canvas.width, canvas.height);
    // Reset composite mode to 'lighter', so that overlapping particles brighten each other.
    variables.context.globalCompositeOperation = 'lighter';
};

// Create particle explosion at 'x' and 'y' coordinates.
const createParticles = (x, y) => {
    // Set particle count.
    // Higher numbers may reduce performance.
    let particleAmount = config.particleCount;
    while (particleAmount--) {
        // Create a new particle and add it to the particles collection.
        variables.particles.push(new Particle(x, y));
    }
};

// Update all active fireworks.
const updateFireworks = () => {
    // Loop backwards through all fireworks, drawing and updating each.
    for (let i = variables.fireworks.length - 1; i >= 0; --i) {
        variables.fireworks[i].draw();
        variables.fireworks[i].update(i);
    }
};

// Update all active particles.
const updateParticles = () => {
    // Loop backwards through all particles, drawing and updating each.
    for (let i = variables.particles.length - 1; i >= 0; --i) {
        variables.particles[i].draw();
        variables.particles[i].update(i);
    }
};

// === END APP HELPERS ===

// Primary loop.
const loop = () => {
    // Smoothly request animation frame for each loop iteration.
    requestAnimFrame(loop);

    // Adjusts coloration of fireworks over time.
    variables.hue += config.hueStepIncrease;

    // Clean the canvas.
    cleanCanvas();

    // Update fireworks.
    updateFireworks();

    // Update particles.
    updateParticles();

    // Launch automated fireworks.
    launchAutomatedFirework();

    // Launch manual fireworks.
    launchManualFirework();

    launchMiddleFirework();

    // Fun, games, and utter chaos
    document.onkeydown = () => {
        switch (window.event.keyCode) {
            case 38:
                config.fireworkSpeed++;
                break;
            case 40:
                if (config.fireworkSpeed >= 1) {
                    config.fireworkSpeed--;
                    break;
                } else {
                    break;
                }
        }
    };

    // Whoopsiedoodles.
    document.onkeyup = () => {
        switch (window.event.keyCode) {
            case 68:
                redButton = true;
                intervalId;
                break;
        }
    };
};

// Initiate the loop after window loads.
window.onload = loop();

// === PARALLAX ===

document.addEventListener('mousemove', parallax);
const elem = document.querySelector('#parallax');
function parallax(e) {
    let _w = window.innerWidth / 2;
    let _h = window.innerHeight / 2;
    let _mouseX = e.clientX;
    let depth1 = `${50 - (_mouseX - _w) * 0.0005}%`;
    let depth2 = `${50 - (_mouseX - _w) * 0.001}%`;
    let x = `${depth2}, ${depth1}`;
    elem.style.backgroundPosition = x;
}
