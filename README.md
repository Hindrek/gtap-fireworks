# README

Steps to get this thing up and running:

1. make sure you have node js installed (version 10 +)
2. run "npm ci"
3. run "npm run build" - this will build files in dist folder and starts watching for file changes.
4. run "npm run start" to start the dev-server.
5. to make changes in the HTML, edit the the file located in src folder, not in dist folder.

# USER GUIDE

1. The Up/Down arrow keys control the speed of the fireworks.
2. Mouse click on the canvas launches a firework towards the cursor.
3. For the love of God, do **NOT** press the D-key!

### Ülesande teekond.

Asjad kulgesid väga põnevalt ja õppisin ülesande käigus väga palju (circular dependencies, what the heck?!).

Kasutasin ilutulestiku loomiseks erinevaid tutorialeid ning ehitasin ülejäänud stseeni nende ümber. Seejärel viisin koodi üle eelnevate projektide juures kasutatud starterprojektile. Refactoring ja debug oli paras põrgu (lesson learned - alusta kohe starteriga) kuid väljusin katsumustest tugevamana kui kunagi varem.

Ajakulu otseselt ei mõõtnud kuid tegelesin projekti eri aspektidega jupikaupa igapäev (sealhulgas lugemismaterjal, erinevate näidete uurimine, katsetamine, stseeni kujundamine, visuaalmaterjali loomine, projekti ülesehitamine).

Soovi korral leiab kiirvaate siit:
https://empty-giraffe.com/impostor-syndrome/

Väljakutse oli küll pisut suurem kui esmapilgul tundus, kuid igav ei hakanud hetkekski.
Loodan, et teil on sama lõbus; have fun!

—Hindrek
